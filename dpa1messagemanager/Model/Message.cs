﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace dpa1messagemanager.Model
{
    class Message
    {
        public int id { get; set; }
        public string name { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
        public string image { get; set; }
        public Uri imageUri { get; set; }
    }
}
