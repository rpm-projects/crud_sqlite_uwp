﻿using dpa1messagemanager.Context;
using dpa1messagemanager.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace dpa1messagemanager
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        ObservableCollection<Message> messageList;
        private Message selectedItem = new Message();
        public StorageFile file;
        public BitmapImage bitImage;
        public string imageName;


        public MainPage()
        {
            this.InitializeComponent();
            //Load messages from local db
            loadFromDb();
        }

        //Load messages from db
        private void loadFromDb()
        {
            using (var db = new MessageContext())
            {
                db.Database.EnsureCreated();
                var test = db.Message.ToList();

                //copy data from image attribute to imageUri in order to bind it on a ibtmap view
                List<Message> doneMessageList = db.Message.ToList();
                foreach (Message msg in doneMessageList)
                {
                    msg.imageUri = new Uri(msg.image);
                }

                messageList = new ObservableCollection<Message>(doneMessageList);
                MessageListView.ItemsSource = messageList;
            }
        }
           
        //Save image in Local Storage
         private async void btnSaveImage_Click(object sender, RoutedEventArgs e)
         {
             //Create An Instance of WriteableBitmap object  
             WriteableBitmap writeableBitmap = new WriteableBitmap(300, 300);

            StorageFolder folder = ApplicationData.Current.LocalFolder;
            if (folder != null)
            {
                FileOpenPicker picker = new FileOpenPicker();

                picker.FileTypeFilter.Add(".jpg");
                picker.FileTypeFilter.Add(".jpeg");
                picker.FileTypeFilter.Add(".png");

                StorageFile file = await picker.PickSingleFileAsync() ;
                if (file == null)
                    return;
                IRandomAccessStream stream = await file.OpenAsync(FileAccessMode.ReadWrite);

                string newName = "";
                int first = 0;
                int last = file.Name.Length;

                string bkpExtesion = file.Name.Substring(last - 4, 4);
                newName = file.Name.Substring(first, last - 4);
                newName = newName + DateTime.Now.Day + DateTime.Now.Month + DateTime.Now.Year + DateTime.Now.Hour + DateTime.Now.Minute;
                newName = newName + bkpExtesion;

                await file.CopyAsync(folder, newName);

                this.imageName = newName;

                BitmapImage bitmapimage = new BitmapImage();
                await bitmapimage.SetSourceAsync(stream);
                picture_view.Source = bitmapimage;

                var encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.JpegEncoderId, stream);
                var pixelStream = writeableBitmap.PixelBuffer.AsStream();
                var pixels = new byte[pixelStream.Length];
                await pixelStream.ReadAsync(pixels, 0, pixels.Length);
                encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore, (uint)writeableBitmap.PixelWidth, (uint)writeableBitmap.PixelHeight, 48, 48, pixels);
                await encoder.FlushAsync();
            }
         }

        //Save message in DB
        private async void Add_Click(object sender, RoutedEventArgs e)
        {
            StorageFolder myfolder = ApplicationData.Current.LocalFolder;
            StorageFile file = await myfolder.GetFileAsync(this.imageName);
            Uri imageUri = new Uri(file.Path);

            using (var db = new MessageContext())
            {
                var message = new Message { name = textBox_name.Text, subject = textBox_subject.Text, message = textBox_message.Text, image = imageUri.ToString() };
                db.Message.Add(message);
                db.SaveChanges();

                MessageListView.ItemsSource = db.Message.ToList();

                string title = "New Message";
                string alertMessage = "Added new message successfully!";
                ToastNotifier ToastNotifier = ToastNotificationManager.CreateToastNotifier();
                Windows.Data.Xml.Dom.XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(ToastTemplateType.ToastText02);
                Windows.Data.Xml.Dom.XmlNodeList toastNodeList = toastXml.GetElementsByTagName("text");
                toastNodeList.Item(0).AppendChild(toastXml.CreateTextNode(title));
                toastNodeList.Item(1).AppendChild(toastXml.CreateTextNode(alertMessage));
                Windows.Data.Xml.Dom.IXmlNode toastNode = toastXml.SelectSingleNode("/toast");
                Windows.Data.Xml.Dom.XmlElement audio = toastXml.CreateElement("audio");
                audio.SetAttribute("src", "ms-winsoundevent:Notification.SMS");

                ToastNotification toast = new ToastNotification(toastXml);
                toast.ExpirationTime = DateTime.Now.AddSeconds(4);
                ToastNotifier.Show(toast);

                //Clean all fields
                textBox_name.Text = "";
                textBox_subject.Text = "";
                textBox_message.Text = "";
                picture_view.Source = new BitmapImage();
            }
        }

        private void Message_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MessageListView.SelectedItem != null)
            {
                selectedItem = MessageListView.SelectedItem as Message;
            }
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (MessageListView.SelectedItem != null)
            {
                using (var db = new MessageContext())
                {
                    db.Message.Remove(MessageListView.SelectedItem as Message);

                    db.SaveChanges();

                    MessageListView.ItemsSource = db.Message.ToList();
                }
            }
        }
    }
}
