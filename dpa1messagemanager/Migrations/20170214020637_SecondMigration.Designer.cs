﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using dpa1messagemanager.Context;

namespace dpa1messagemanager.Migrations
{
    [DbContext(typeof(MessageContext))]
    [Migration("20170214020637_SecondMigration")]
    partial class SecondMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("dpa1messagemanager.Model.Message", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("image");

                    b.Property<string>("message");

                    b.Property<string>("name")
                        .IsRequired();

                    b.Property<string>("subject");

                    b.HasKey("id");

                    b.ToTable("Message");
                });
        }
    }
}
