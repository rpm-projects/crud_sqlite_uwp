﻿using dpa1messagemanager.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace dpa1messagemanager.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NewItemScreen : Page
    {
        public StorageFile file;
        public BitmapImage bitImage;


        public NewItemScreen()
        {
            this.InitializeComponent();
        }

        public void save()
        {

        }

        private async void uploadImage(object sender, RoutedEventArgs e)
        {
            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;

            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".jpeg");
            openPicker.FileTypeFilter.Add(".png");

             file = await openPicker.PickSingleFileAsync();

            if (file != null)
            {
                var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                bitImage = new BitmapImage();
                bitImage.SetSource(stream);
                picture_view.Source = bitImage;
                        
            }
        }


        //Go back to main screen
        public void backToMainScreen(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        //Save and go back to main screen
        public void saveAndBackToMainScreen(object sender, RoutedEventArgs e)
        {
            Message msg = new Message();
            msg.name = textBox_name.Text;
            msg.subject = textBox_subject.Text;
            msg.message = textBox_message.Text;
            //msg.image = bitImage;

            //StorageFile MyFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///test.jpg"));
            this.Frame.Navigate(typeof(MainPage), msg);
        }
        
    }
}   
