﻿using dpa1messagemanager.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dpa1messagemanager.ViewModel
{
    class MessageViewModel
    {
        ObservableCollection<Message> MessageList = new ObservableCollection<Message>();

        public ObservableCollection<Message> getListMessage()
        {
            //Add 20 messages to the list
            for(int i = 0; i < 20; i++)
            {
                Message msg = new Message();
                msg.name = "Title " + i;
                msg.subject = "Mobile " + i;
                //msg.imagePath = "/Images/user.jpg";
                msg.message = "Test " + i + "bla bla bla";

                MessageList.Add(msg);
            }

            return MessageList;
        }

    }
}
