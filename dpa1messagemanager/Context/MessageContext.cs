﻿using dpa1messagemanager.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dpa1messagemanager.Context
{
    class MessageContext: DbContext
    {
        internal DbSet<Message> Message { get; set; }

        protected override void OnConfiguring
        (DbContextOptionsBuilder optionsBuilder)
            {
                optionsBuilder.UseSqlite("Filename=Messages.db");
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                // Make Id required.
                modelBuilder.Entity<Message>()
                    .Property(m => m.id)
                    .IsRequired();

            modelBuilder.Entity<Message>()
                .Property(b => b.id)
                .ValueGeneratedOnAdd();

           // Make Name required.
           modelBuilder.Entity<Message>()
                    .Property(m => m.name)
                    .IsRequired();

            // Make Name required.
            modelBuilder.Entity<Message>()
                .Property(m => m.image);

            modelBuilder.Entity<Message>()
                .Ignore(m => m.imageUri);
        }
    }
}
